// Dependencies
import Vue from 'vue'

// Plugins
import vuetify from './plugins/vuetify';

// Main app itself
import App from './components/App.vue'

// Global objects
import store from './globals/store';
import router from './globals/router';

Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app');
