import Vue from "vue";
import VueRouter from "vue-router";
import Learner from "../components/learner/Learner";
import Recognizer from "../components/recognizer/Recognizer";
import CharRecManager from "../components/char-rec-manager/CharRecManager";

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        { name: 'learner', path: '/learn', component: Learner },
        { name: 'recognizer', path: '/recognize', component: Recognizer },
        { name: 'char-rec-manager', path: '/manage-character-recognizer', component: CharRecManager },
        { path: '/', redirect: {name: 'learner'} }
    ]
});