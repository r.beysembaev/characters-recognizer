import {get} from "./httpService";
import {generateApi} from "./neuroApiFactory";

// Common
export const API_URL = 'http://localhost:8080/api';
export const ASSETS_UPLOAD = '/assets/upload';
export const ASSETS_FIND = '/assets/find';
export const ASSETS_DELETE = '/assets/delete';
export const NETWORK_CREATE = '/network/create';

// Character recognition
const CHARACTER_RECOGNITION = '/character-recognizing';
const GET_PICTURE_FILE = CHARACTER_RECOGNITION + '/assets/get-file';
const GET_ALPHABET = CHARACTER_RECOGNITION + '/network/get-alphabet';
const GET_FLAGS = CHARACTER_RECOGNITION + '/network/get-flags';

export default {

    characterRecognizing: generateApi(CHARACTER_RECOGNITION, {

        assets: {
            getUrl: id => API_URL + GET_PICTURE_FILE + '?id=' + id
        },

        network: {
            getAlphabet: () => get(GET_ALPHABET),
            getFlags: () => get(GET_FLAGS)
        }

    })

}

