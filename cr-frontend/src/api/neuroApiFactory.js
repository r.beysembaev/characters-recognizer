import {get, post, postWithFiles} from "./httpService";
import {ASSETS_DELETE, ASSETS_FIND, ASSETS_UPLOAD, NETWORK_CREATE} from "./Api";

export function generateApi(controllerUrl, extraMethods) {
    return {

        ...extraMethods,

        assets: {

            upload: (files, data) => postWithFiles(controllerUrl + ASSETS_UPLOAD, files, data),

            delete: assetIds => post(controllerUrl + ASSETS_DELETE, assetIds),

            find: (page, size) => {
                size = size < 0 ? 100 : size;
                return get(ASSETS_FIND, { page: page - 1, size, sort: 'id,desc' })
            },

            ...extraMethods.assets
        },

        network: {

            create: data => post(controllerUrl + NETWORK_CREATE, data),

            ...extraMethods.network
        }
    }
}