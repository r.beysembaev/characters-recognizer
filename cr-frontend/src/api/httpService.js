import axios from "axios";
import {API_URL} from "./Api";

export function postWithFiles(methodUrl, files, data) {

    let formData = new FormData();

    files.forEach(file => formData.append('files', file));
    if (data) {
        for (let [key, value] of Object.entries(data)) {
            formData.append(key, value);
        }
    }

    return new Promise((resolve, reject) => {
        axios.post( API_URL + methodUrl,
            formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                withCredentials: true
            }
        ).then(response => {

            resolve(response.data);

        }).catch(response => {

                console.warn('API call failed! Method: ' + methodUrl + '. See request data and response below',
                    data, response);
                reject(response);
            }
        );
    });
}

export function get(methodUrl, params) {

    params = params || {};

    return new Promise((resolve, reject) => {
        axios.get(API_URL + methodUrl,
            {
                params,
                withCredentials: true
            })

            .then(response => {
                resolve(response.data.result);
            })

            .catch(response => {
                console.warn('API call failed! Method: ' + methodUrl + '. See request data and response below',
                    params, response);
                reject(response);
            });
    });
}

export function post(methodUrl, params) {

    return new Promise((resolve, reject) => {
        axios.post(API_URL + methodUrl,
            params,
            {
                withCredentials: true
            })

            .then(response => {
                console.log(response)
                resolve(response.data.result);
            })

            .catch(response => {
                console.warn('API call failed! Method: ' + methodUrl + '. See request data and response below',
                    params, response);
                reject(response);
            });
    });
}