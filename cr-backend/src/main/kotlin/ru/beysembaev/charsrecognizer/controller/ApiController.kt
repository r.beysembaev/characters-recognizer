package ru.beysembaev.charsrecognizer.controller

import org.springframework.web.bind.annotation.ExceptionHandler
import ru.beysembaev.charsrecognizer.dto.Failure

abstract class ApiController {

    @ExceptionHandler(value = [Exception::class])
    fun error(ex: Exception): Failure {
        ex.printStackTrace()
        return ERROR
    }

    companion object {
        val ERROR: Failure = Failure("Internal server error")
    }
}