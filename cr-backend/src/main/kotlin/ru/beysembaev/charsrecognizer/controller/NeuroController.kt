package ru.beysembaev.charsrecognizer.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import ru.beysembaev.charsrecognizer.dto.Status
import ru.beysembaev.charsrecognizer.dto.Success
import ru.beysembaev.charsrecognizer.neuro.ClassRoom
import ru.beysembaev.charsrecognizer.neuro.LearningAsset
import ru.beysembaev.charsrecognizer.neuro.Neuron
import ru.beysembaev.charsrecognizer.service.BaseNeuroService

abstract class NeuroController<N: Neuron, L: LearningAsset, S: BaseNeuroService<N>> constructor(
        open val classRoom: ClassRoom<N, L>
) {
    @PostMapping("/assets/delete")
    fun deleteAssetsMethod(@RequestBody assetIds: List<Long>): Status {
        deleteAssets(assetIds)
        return Success()
    }

    @GetMapping("/network/teach")
    fun startLearner(): Status {
        classRoom.startLearning()
        return Success()
    }

    // TODO create an abstract BaseNeuroService having the common methods
    protected abstract fun deleteAssets(assetIds: List<Long>)
}