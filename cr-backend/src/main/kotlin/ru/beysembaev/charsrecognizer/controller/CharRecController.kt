package ru.beysembaev.charsrecognizer.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import ru.beysembaev.charsrecognizer.dto.PageDTO
import ru.beysembaev.charsrecognizer.dto.Status
import ru.beysembaev.charsrecognizer.dto.Success
import ru.beysembaev.charsrecognizer.model.CharRecNeuron
import ru.beysembaev.charsrecognizer.model.CharacterPicture
import ru.beysembaev.charsrecognizer.neuro.CharacterRecognition.CharRecClassRoom
import ru.beysembaev.charsrecognizer.service.CharRecService
import ru.beysembaev.charsrecognizer.service.FileService
import ru.beysembaev.charsrecognizer.util.mapper.CharacterPictureMapper
import ru.beysembaev.charsrecognizer.util.mapper.FlagsMapper
import javax.servlet.http.HttpServletResponse

@RestController
@RequestMapping("/api/character-recognizing")
@ResponseStatus(HttpStatus.OK)
class CharRecController @Autowired constructor(

        override val classRoom: CharRecClassRoom,
        val fileService: FileService,
        val charRecService: CharRecService,
        val characterPictureMapper: CharacterPictureMapper,
        val flagsMapper: FlagsMapper

        ): NeuroController<CharRecNeuron, CharacterPicture, CharRecService>(classRoom) {

    class NetworkForm(var alphabet: List<Char>,
                      var hiddenLayersNumber: Int,
                      var neuronsPerLayer: Int,
                      var picWidth: Int,
                      var picHeight: Int)



    override fun deleteAssets(assetIds: List<Long>) {
        charRecService.deletePictures(assetIds)
    }


    // TODO write a custom ExceptionHandler to return code 404 if the file is not found
    @GetMapping("/assets/get-file")
    fun getPictureFile(@RequestParam(value = "id", required = true) id: Long, response: HttpServletResponse): ByteArray {
        response.setHeader("Content-Disposition", "attachment;filename=\"$id.png\"")
        return fileService.getPicture(id)
    }


    // TODO upload by reading the image and saving it as png
    @PostMapping("/assets/upload")
    fun uploadPictures(
            @RequestParam("character", required = true) character: Char,
            @RequestParam("files", required = true) files: List<MultipartFile>
    ): Status {
        //charRecService.savePictures(files.map { it.inputStream })
        return Success()
    }


    // TODO add some limits to the pageable's values
    @GetMapping("/assets/find")
    fun findPictures(@PageableDefault(value = 10, page = 0) pageable: Pageable): Status {

        val picturesPage = charRecService.findPictures(pageable)
        val pictureDtos = characterPictureMapper.toDTOs(picturesPage.content)
        // TODO I don't know Rick, 'as Any' looks shitty
        return Success(PageDTO.create(pictureDtos,
                picturesPage.number.toLong(), picturesPage.size.toLong(), picturesPage.totalElements) as Any)

    }


    @GetMapping("/network/get-alphabet")
    fun getAlphabet(): Status {
        return Success(charRecService.getAlphabet())
    }


    // TODO validate, replace NetworkForm with request params
    @PostMapping("/network/create")
    fun createNetwork(@RequestBody networkForm: NetworkForm): Status {
        return Success(charRecService.createNetwork(networkForm.alphabet, networkForm.hiddenLayersNumber,
                networkForm.neuronsPerLayer, networkForm.picWidth, networkForm.picHeight))
    }


    @GetMapping("/network/get-flags")
    fun getFlags(): Status {
        val flags = flagsMapper.toDTO(charRecService.getFlags())
        return Success(flags)
    }
}