package ru.beysembaev.charsrecognizer.service.impl

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.beysembaev.charsrecognizer.model.CharacterPicture
import ru.beysembaev.charsrecognizer.service.FileService
import java.io.File
import java.io.FileNotFoundException
import java.io.InputStream
import javax.annotation.PostConstruct

// TODO make it more general. Instead of getPicture use getFile etc
@Service
class FileServiceImpl (

        @Value("\${files.dir}") val dir: String,
        @Value("\${files.pictures.extension}") val extension: String

): FileService {

    @PostConstruct
    private fun init() {
        val folder = File(dir)
        if (!folder.exists()) {
            folder.mkdirs()
        }
    }

    override fun getPicture(id: Long): ByteArray {
        val path = String.format("%s/%s.%s", dir, id, extension)
        val file = File(path)
        if (!file.isFile) {
            throw FileNotFoundException()
        }

        return file.readBytes()
    }

    override fun savePicture(picture: CharacterPicture, bytes: ByteArray) {
        val path = String.format("%s/%s.%s", dir, picture.id, extension)
        val file = File(path)
        if (!file.createNewFile()) {
            throw IllegalArgumentException("File already exists!")
        }

        file.writeBytes(bytes)
    }

    override fun getFile(fileName: String): InputStream? {
        val file = File("$dir/$fileName")
        return if (file.isFile) file.inputStream() else null
    }

    override fun deleteFile(fileName: String): Boolean {
        val file = File("$dir/$fileName")
        return file.isFile && file.delete()
    }
}