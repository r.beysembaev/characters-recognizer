package ru.beysembaev.charsrecognizer.service

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import ru.beysembaev.charsrecognizer.model.CharRecNeuron
import ru.beysembaev.charsrecognizer.model.CharacterPicture
import ru.beysembaev.charsrecognizer.model.Flags
import java.io.InputStream

// TODO move methods to the base interface where possible
interface CharRecService: BaseNeuroService<CharRecNeuron> {
    fun findAllNeurons(): List<CharRecNeuron>
    fun savePictures(inputStreams: List<InputStream>)
    fun findPictures(pageable: Pageable): Page<CharacterPicture>
    fun findAllPictures(): List<CharacterPicture>
    fun getPictureFile(id: Long): InputStream?
    fun deletePictures(pictureIds: List<Long>)
    fun getAlphabet(): List<Char>
    fun createNetwork(alphabet: List<Char>,
                      hiddenLayersNumber: Int,
                      neuronsPerLayer: Int,
                      picWidth: Int,
                      picHeight: Int)
    fun getFlags(): Flags
}