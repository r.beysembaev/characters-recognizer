package ru.beysembaev.charsrecognizer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class CharsRecognizerApplication

fun main(args: Array<String>) {
	runApplication<CharsRecognizerApplication>(*args)
}