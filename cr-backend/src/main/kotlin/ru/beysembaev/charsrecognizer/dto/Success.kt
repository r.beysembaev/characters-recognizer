package ru.beysembaev.charsrecognizer.dto

class Success: Status {
    constructor(): super(true, null)
    constructor(result: Any): super(true, result)
}
