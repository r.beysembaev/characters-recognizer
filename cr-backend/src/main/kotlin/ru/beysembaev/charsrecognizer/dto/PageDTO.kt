package ru.beysembaev.charsrecognizer.dto

class PageDTO<T> (val from: Long,
                  val to: Long,
                  val total: Long,
                  val pageNumber: Long,
                  val pageSize: Long,
                  val items: List<T>,
                  val totalPages: Long) {

    // TODO can we get rid of this with some smart mapping of Page?
    companion object {
        @JvmStatic
        fun <T> create(items: List<T>, pageNumber: Long, pageSize: Long, total: Long): PageDTO<T>? {
            return PageDTO(
                    from = Math.min((pageNumber - 1) * pageSize, total),
                    to = Math.min(pageNumber * pageSize, total),
                    total = total,
                    items = items,
                    pageNumber = pageNumber,
                    pageSize = pageSize,
                    totalPages = Math.ceil(total / pageSize.toDouble()).toLong())
        }
    }
}