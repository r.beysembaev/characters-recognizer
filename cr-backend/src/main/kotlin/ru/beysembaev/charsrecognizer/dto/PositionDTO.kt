package ru.beysembaev.charsrecognizer.dto

import com.github.pozo.KotlinBuilder

@KotlinBuilder
data class PositionDTO(
        var x: Int?,
        var y: Int?
)