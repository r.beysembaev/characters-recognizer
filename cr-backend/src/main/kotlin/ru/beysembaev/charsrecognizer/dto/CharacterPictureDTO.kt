package ru.beysembaev.charsrecognizer.dto

import com.github.pozo.KotlinBuilder

@KotlinBuilder
data class CharacterPictureDTO(var id: Long)