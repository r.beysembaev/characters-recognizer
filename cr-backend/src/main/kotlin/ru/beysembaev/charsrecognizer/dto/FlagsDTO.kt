package ru.beysembaev.charsrecognizer.dto

import com.github.pozo.KotlinBuilder

@KotlinBuilder
data class FlagsDTO (
        var charRecNetworkCreated: Boolean?,
        var charRecNetworkTaught: Boolean?
)