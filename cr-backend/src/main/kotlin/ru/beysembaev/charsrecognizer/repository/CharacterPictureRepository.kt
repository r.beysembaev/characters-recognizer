package ru.beysembaev.charsrecognizer.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import ru.beysembaev.charsrecognizer.model.CharacterPicture

@Repository
interface CharacterPictureRepository: JpaRepository<CharacterPicture, Long>, JpaSpecificationExecutor<CharacterPicture> {
    fun findByHash(hash: Int): List<CharacterPicture>
}