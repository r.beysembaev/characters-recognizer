package ru.beysembaev.charsrecognizer.model

import com.github.pozo.KotlinBuilder
import ru.beysembaev.charsrecognizer.neuro.Neuron
import javax.persistence.*

@Entity
@Table(name = "cr_character_recognition_neuron")
@KotlinBuilder
data class CharRecNeuron (

        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "character_recognition_neuron_id_gen")
        @SequenceGenerator(name = "character_recognition_neuron_id_gen", sequenceName = "character_recognition_neuron_id_seq", allocationSize = 1)
        override var id: Long = 0,

        @Column(name = "layer")
        override var layer: Int = 0,

        @Column(name = "position")
        override var position: Int = 0,

        @Transient
        override var inputs: List<CharRecSynapse> = ArrayList(),

        @Transient
        override var outputs: List<CharRecSynapse> = ArrayList()

): Neuron