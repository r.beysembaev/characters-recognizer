package ru.beysembaev.charsrecognizer.model

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "cr_flags")
data class Flags (

        @Id
        val id: Int = 1,

        @Column(name = "char_rec_network_created")
        var charRecNetworkCreated: Boolean = false,

        @Column(name = "char_rec_network_taught")
        var charRecNetworkTaught: Boolean = false

)