package ru.beysembaev.charsrecognizer.model

import ru.beysembaev.charsrecognizer.neuro.NeuronMeaning
import javax.persistence.*

@Entity
@Table(name = "cr_character_recognition_output_meaning")
// TODO add indexes
data class CharRecOutputMeaning (

        @Column(name = "neuron")
        @Id
        override var neuronId: Long,

        @Column(name = "meaning")
        override var meaning: Char

): NeuronMeaning<Char>