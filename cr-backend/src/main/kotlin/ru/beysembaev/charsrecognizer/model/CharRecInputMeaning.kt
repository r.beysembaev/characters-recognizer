package ru.beysembaev.charsrecognizer.model

import ru.beysembaev.charsrecognizer.neuro.NeuronMeaning
import javax.persistence.*

@Entity
@Table(name = "cr_character_recognition_input_meaning")
// TODO add indexes
data class CharRecInputMeaning (

        @Column(name = "neuron")
        @Id
        override var neuronId: Long,

        @Embedded
        override var meaning: Position

): NeuronMeaning<Position>