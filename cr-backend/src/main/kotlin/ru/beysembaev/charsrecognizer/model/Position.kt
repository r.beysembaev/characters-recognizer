package ru.beysembaev.charsrecognizer.model

import com.github.pozo.KotlinBuilder
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
@KotlinBuilder
data class Position (

        @Column(name = "position_x", nullable = false)
        var x: Int = 0,

        @Column(name = "position_y", nullable = false)
        var y: Int = 0
)