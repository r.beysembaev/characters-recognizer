package ru.beysembaev.charsrecognizer.model

import com.github.pozo.KotlinBuilder
import ru.beysembaev.charsrecognizer.neuro.Synapse
import javax.persistence.*

// TODO add constraint of unique neuron + position
@Entity
@Table(name = "cr_character_recognition_synapse")
@KotlinBuilder
data class CharRecSynapse (

        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "character_recognition_synapse_id_gen")
        @SequenceGenerator(name = "character_recognition_synapse_id_gen", sequenceName = "character_recognition_synapse_id_seq", allocationSize = 1)
        override var id: Long = 0,

        @Column(name = "weight")
        override var weight: Double = 0.0,

        @Column(name = "bias")
        override var bias: Double = 0.0,

        @Column(name = "neuron_from_id")
        override var neuronFromId: Long,

        @Column(name = "neuron_to_id")
        override var neuronToId: Long,

        // TODO change to CharRecNeuron = CharRecNeuron()?
        @Transient
        override var neuronFrom: CharRecNeuron? = null,

        @Transient
        override var neuronTo: CharRecNeuron? = null

): Synapse<CharRecNeuron>