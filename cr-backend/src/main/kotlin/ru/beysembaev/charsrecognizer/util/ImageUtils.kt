package ru.beysembaev.charsrecognizer.util

import java.awt.image.BufferedImage

object ImageUtils {
    fun getPixelGrayscaleValue(img: BufferedImage, x: Int, y: Int): Int {
        return getGrayscaleValue(img.getRGB(x, y))
    }

    fun getGrayscaleValue(rgb: Int): Int {
        return (getRed(rgb) + getGreen(rgb) + getBlue(rgb)) / 3
    }

    fun getRed(rgb: Int): Int {
        return (rgb ushr 16) and 0xFF
    }

    fun getGreen(rgb: Int): Int {
        return (rgb ushr 8) and 0xFF
    }

    fun getBlue(rgb: Int): Int {
        return (rgb ushr 0) and 0xFF
    }
}