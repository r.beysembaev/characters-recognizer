package ru.beysembaev.charsrecognizer.util.mapper

import org.mapstruct.Mapper
import ru.beysembaev.charsrecognizer.dto.FlagsDTO
import ru.beysembaev.charsrecognizer.model.Flags

@Mapper(componentModel = "spring")
interface FlagsMapper {
    fun toDTO(source: Flags): FlagsDTO
    fun toModel(source: FlagsDTO): Flags
}