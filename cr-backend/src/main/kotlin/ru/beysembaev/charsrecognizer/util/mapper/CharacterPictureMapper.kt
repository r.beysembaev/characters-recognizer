package ru.beysembaev.charsrecognizer.util.mapper

import org.mapstruct.Mapper
import ru.beysembaev.charsrecognizer.dto.CharacterPictureDTO
import ru.beysembaev.charsrecognizer.model.CharacterPicture

@Mapper(componentModel = "spring")
interface CharacterPictureMapper {
    fun toDTO(source: CharacterPicture): CharacterPictureDTO
    fun toModel(source: CharacterPictureDTO): CharacterPicture
    fun toDTOs(source: List<CharacterPicture>): List<CharacterPictureDTO>
    fun toModels(source: List<CharacterPictureDTO>): List<CharacterPicture>
}