package ru.beysembaev.charsrecognizer.util.mapper

import org.mapstruct.Mapper
import ru.beysembaev.charsrecognizer.dto.PositionDTO
import ru.beysembaev.charsrecognizer.model.Position

@Mapper(componentModel = "spring")
interface PositionMapper {
    fun toDTO(source: Position): PositionDTO
    fun toModel(source: PositionDTO): Position
    fun toDTOs(source: List<Position>): List<PositionDTO>
    fun toModels(source: List<PositionDTO>): List<Position>
}