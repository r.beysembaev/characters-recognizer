package ru.beysembaev.charsrecognizer.util

// TODO replace with settings stored in DB
class TempSettings {
    companion object {
        const val PICTURE_WIDTH = 28
        const val PICTURE_HEIGHT = 28
        const val HIDDEN_LAYERS_NUMBER = 2
        const val HIDDEN_LAYERS_NEURON_NUMBER = 8
    }
}