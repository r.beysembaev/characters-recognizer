package ru.beysembaev.charsrecognizer.neuro

interface Synapse<T: Neuron> {
    val id: Long
    var weight: Double
    var bias: Double
    val neuronFromId: Long
    val neuronToId: Long
    var neuronFrom: T?
    var neuronTo: T?
}