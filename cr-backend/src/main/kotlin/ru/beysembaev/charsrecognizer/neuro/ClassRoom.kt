package ru.beysembaev.charsrecognizer.neuro

abstract class ClassRoom<N: Neuron, L: LearningAsset> {
    // TODO use LinkedList?
    var neurons = ArrayList<N>()
    var asset = ArrayList<L>()

    abstract fun loadNeurons(): List<N>
    abstract fun loadAssets(): List<L>

    fun startLearning() {

    }
}