package ru.beysembaev.charsrecognizer.neuro

interface Neuron {
    val id: Long
    val layer: Int
    val position: Int
    // TODO it's not just T out Neuron, it's 'this' class
    val inputs: List<Synapse<out Neuron>>
    val outputs: List<Synapse<out Neuron>>
}