package ru.beysembaev.charsrecognizer.neuro

interface NeuronMeaning<T> {
    val neuronId: Long
    val meaning: T
}